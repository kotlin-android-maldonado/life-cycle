package com.example.lifecycle

import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.AttributeSet
import android.util.Log
import android.view.View
import com.example.lifecycle.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private var mediaPlayer: MediaPlayer? = null
    private var position: Int = 0
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnCheck.setOnClickListener {
            startActivity(Intent(this, DiaologActivity::class.java))
        }
        Log.i("LiveCycle", "OnCreate")
    }

    override fun onCreateView(name: String, context: Context, attrs: AttributeSet): View? {
        Log.i("LiveCycle", "OnCreateView")
        return super.onCreateView(name, context, attrs)
    }

    override fun onStart() {
        super.onStart()
        mediaPlayer = MediaPlayer.create(this, R.raw.ai_2)
        Log.i("LiveCycle", "OnStart")
    }

    override fun onResume() {
        super.onResume()
        mediaPlayer?.seekTo(position)
        mediaPlayer?.start()
        Log.i("LiveCycle", "OnResume")
    }

    override fun onPause() {
        super.onPause()
        mediaPlayer?.pause()
        mediaPlayer?.let {
            position = it.currentPosition
        }
        Log.i("LiveCycle", "OnPause")
    }

    override fun onStop() {
        super.onStop()
        mediaPlayer?.stop()
        mediaPlayer?.release()
        mediaPlayer = null
        Log.i("LiveCycle", "OnStop")
    }

    override fun onRestart() {
        super.onRestart()
        Log.i("LiveCycle", "OnRestart")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("LiveCycle", "OnDestroy")
    }
}